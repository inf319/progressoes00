package engsoft;

import java.util.ArrayList;

public class ProgressaoAritmetica extends Progressao {
	private int incremento;
	
	public ProgressaoAritmetica() {
		setProgressao(new ArrayList<Integer>());
		setIncremento(1);
	}
	
	public ProgressaoAritmetica(int incremento) {
		setProgressao(new ArrayList<Integer>());
		setIncremento(incremento);
	}

	public int getIncremento() {
		return incremento;
	}

	public void setIncremento(int incremento) {
		this.incremento = incremento;
	}

	@Override
	public int calculaProgressaoDaPosicao(int posicao) {
		if (posicao == 0) {
			return 0;
		} else {
			return calculaProgressaoDaPosicao(posicao - 1) + getIncremento();
		}
	}
}
