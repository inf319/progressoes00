package engsoft;

import java.util.List;

public abstract class Progressao {
	private List<Integer> progressao;
	
	public List<Integer> getProgressao() {
		return progressao;
	}

	public void setProgressao(List<Integer> progressao) {
		this.progressao = progressao;
	}

	/**
	 * Método que inicia uma progressão
	 * 
	 * @return O primeiro termo
	 */
	public int inicia() {
		this.getProgressao().add(calculaProgressaoDaPosicao(0));
		return calculaProgressaoDaPosicao(0);
	}
	
	/**
	 * Método que retorna o próximo termo da progressão
	 * 
	 * @return o valor do próximo termo
	 */
	public int proxTermo() {
		int posicao = this.getProgressao().size();
		this.getProgressao().add(calculaProgressaoDaPosicao(posicao));
		
		return this.getProgressao().get(this.getProgressao().size() - 1);
	}
	
	/**
	 * Método para pegar o iesimo termo da progressão
	 * 
	 * @param posicao
	 * @return o iesimo termo
	 */
	public int iesimoTermo(int posicao) {
		return calculaProgressaoDaPosicao(posicao);
	}
	
	/**
	 * Método para imprimir uma progressão de n termos
	 * 
	 * @param termos
	 * @return Uma String formatada
	 */
	public String imprimeProgressao(int termos) {
		StringBuilder lista = new StringBuilder();
		
		for (int i = 0; i <= termos; i++) {
			lista.append(" " + calculaProgressaoDaPosicao(i));
		}
		lista.deleteCharAt(0);
		lista.append("\n");
		
		return lista.toString();
	}
	
	/**
	 * Método abstrato que deve ser implementado para calcular uma progressão
	 * 
	 * @param posicao
	 * @return O termo de uma determinada posicao
	 */
	public abstract int calculaProgressaoDaPosicao(int posicao);
}
