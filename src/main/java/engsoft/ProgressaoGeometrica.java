package engsoft;

import java.util.ArrayList;

public class ProgressaoGeometrica extends Progressao {
	private int base;
	
	public ProgressaoGeometrica() {
		setProgressao(new ArrayList<Integer>());
		setBase(2);
	}
	
	public ProgressaoGeometrica(int base) {
		setProgressao(new ArrayList<Integer>());
		setBase(base);
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	@Override
	public int calculaProgressaoDaPosicao(int posicao) {
		if (posicao == 0) {
			return 1;
		} else {
			return calculaProgressaoDaPosicao(posicao - 1) * getBase();
		}
	}
}
